class RemoveContentFromActivityLog < ActiveRecord::Migration[7.1]
  def change
    remove_column :activity_logs, :content, :text
  end
end
