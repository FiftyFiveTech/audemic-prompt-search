require 'json'
require 'net/http'
require 'elasticsearch/model'
require_relative '../lib/elasticsearch_client'

# Main class to coordinate bulk import process
class PromptImportCoordinator
  def initialize(hugging_face_url)
    @hugging_face_url = hugging_face_url
  end

  def perform_import
    # Fetch prompts from the external source
    prompts = ElasticSearch::PromptUpdaterService.new(@hugging_face_url).import_prompts
  end
end

# Configuration 
hugging_face_url = Rails.application.config.hugging_face_url

# Initiate the import process
PromptImportCoordinator.new(hugging_face_url).perform_import
