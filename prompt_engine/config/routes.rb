Rails.application.routes.draw do
  get "/prompts/search", to: "prompts#search"
  get "/prompts", to: "prompts#retrieve_all"

  root "prompts#retrieve_all"
  # Catch-all unmatched routes
  match '*unmatched', to: 'application#not_found_routes', via: :all
end