$DEFAULT_DATA_SIZE= ENV["DEFAULT_DATA_SIZE"]
$PAGINATION_SIZE= ENV["PAGINATION_SIZE"]
$LIST_PROMPTS_FAILED='Failed to list prompts'
$PERFORM_SEARCH_FAILED='Failed to perform search'
$IMPORT_PROMPTS_FAILED='Failed to import prompts to ElasticSearch'
$DELETE_PROMPTS_FAILED='Failed to delete prompts from ElasticSearch'
$PROMPT_UPDATER_FAILED='Failed to update prompts'
$PERFORM_INDEXING_FAILED='Failed to perform indexing on ElasticSearch'