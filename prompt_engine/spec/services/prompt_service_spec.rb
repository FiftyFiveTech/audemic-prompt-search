require_relative '../rails_helper'
require 'will_paginate/array'

# Describe the PromptService class, specifying it as a service type
RSpec.describe PromptService, type: :service do
  let(:prompt_repository) { instance_double('PromptRepository') }
  let(:client) { double('Elasticsearch::Client') }
  let(:prompts) { ['Prompt 1', 'Prompt 2'] }
  let(:page) { 1 }
  let(:search_query) { 'search_query' }

  # Create an instance of PromptService
  subject { described_class.new(prompt_repository) }

  # Describe the list_prompts method
  describe '#list_prompts' do
    # Test case description
    it 'fetches initial data and paginates' do
      # Expect list to be called and return prompts
      expect(prompt_repository).to receive(:list).and_return(prompts)
      expect(subject).to receive(:paginate).with(prompts, page)

      subject.list_prompts(page)
    end

    # Test case for error handling
    it 'raises error if fetching data fails' do
      # Mock list to raise an error
      allow(prompt_repository).to receive(:list).and_raise(StandardError)
      # Expect an error to be raised when list_prompts is called
      expect { subject.list_prompts(page) }.to raise_error(StandardError)
    end
  end

  # Describe the perform_search method
  describe '#perform_search' do
    # Test case description
    it 'performs search and paginates' do
      expect(prompt_repository).to receive(:find).with(search_query).and_return(prompts)
      expect(subject).to receive(:paginate).with(prompts, page)

      subject.perform_search(search_query, page)
    end

    # Test case for error handling
    it 'raises error if search fails' do
      allow(prompt_repository).to receive(:find).and_raise(StandardError)
      expect { subject.perform_search(search_query, page) }.to raise_error(StandardError)
    end
  end

  # Describe the import_prompts method
  describe '#import_prompts' do
    # Test case description
    it 'imports prompts to Elasticsearch' do
      expect(subject).to receive(:build_bulk_request_body).with(prompts).and_return('bulk_request_body')
      expect(prompt_repository).to receive(:save).with('bulk_request_body')

      subject.import_prompts(prompts)
    end

    # Test case for error handling
    it 'raises error if import fails' do
      # Mock build_bulk_request_body to raise an error
      allow(subject).to receive(:build_bulk_request_body).and_raise(StandardError)
      # Expect an error to be raised when import_prompts is called
      expect { subject.import_prompts(prompts) }.to raise_error(StandardError)
    end
  end

  # Describe the delete method
  describe '#delete' do
    # Test case description
    it 'deletes existing prompts from Elasticsearch' do
      expect(prompt_repository).to receive(:delete).with('index_name')

      subject.delete_prompts('index_name')
    end

    # Test case for error handling
    it 'raises error if deletion fails' do
      # Mock delete to raise an error
      allow(prompt_repository).to receive(:delete).and_raise(StandardError)
      # Expect an error to be raised when delete is called
      expect { subject.delete('index_name') }.to raise_error(StandardError)
    end
  end
end
