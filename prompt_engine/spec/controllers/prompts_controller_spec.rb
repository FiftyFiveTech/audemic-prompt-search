require_relative "../rails_helper"

# Describe the PromptsController, specifying it as a controller type
RSpec.describe PromptsController, type: :controller do
  let(:prompt_service) { instance_double('PromptService') }

  # Before each test case
  before do
    # Mock the setter method
    allow(controller).to receive(:setter)
    # Mock the prompt_service method to return the prompt_service double
    allow(controller).to receive(:prompt_service).and_return(prompt_service)
    # Mock the perform_later method of PromptSearchTrackerJob
    allow(PromptSearchTrackerJob).to receive(:perform_later)
  end

  # Describe the GET #retrieve_all action
  describe 'GET #retrieve_all' do
    # Context when prompts are found
    context 'when prompts are found' do
      let(:prompts) { ['Prompt 1', 'Prompt 2'] }

      # Before each test case in this context
      before do
        allow(prompt_service).to receive(:list_prompts).with('1').and_return(prompts)
      end

      # Test case description
      it 'renders search template with status ok' do
        get :retrieve_all, params: { page: '1' }
        expect(response).to render_template('index')
        expect(response).to have_http_status(:ok)
      end
    end

    # Context when prompts are not found
    context 'when prompts are not found' do
      # Before each test case in this context
      before do
        allow(prompt_service).to receive(:list_prompts).with('1').and_return([])
      end

      # Test case description
      it 'renders search template with status not found' do
        get :retrieve_all, params: { page: '1' }
        expect(response).to render_template('index')
        expect(response).to have_http_status(:not_found)
      end
    end

    # Context when an error occurs
    context 'when an error occurs' do
      # Before each test case in this context
      before do
        allow(prompt_service).to receive(:list_prompts).and_raise(PromptServiceException)
      end

      # Test case description
      it 'renders search template with flash alert' do
        get :retrieve_all, params: { page: '1' }
        expect(flash[:alert]).to be_present
        expect(response).to render_template('index')
      end
    end
  end

  # Describe the GET #search action
  describe 'GET #search' do
    # Context with valid search query
    context 'with valid search query' do
      let(:prompts) { ['Prompt 1', 'Prompt 2'] }

      # Before each test case in this context
      before do
        allow(prompt_service).to receive(:perform_search).with('query', '1').and_return(prompts)
      end

      # Test case description
      it 'renders search template with prompts' do
        get :search, params: { page: '1', query: 'query' }
        expect(assigns(:prompts)).to eq(prompts)
        expect(response).to render_template('prompts/index')
      end
    end

    # Context with no search results
    context 'with no search results' do
      # Before each test case in this context
      before do
        allow(prompt_service).to receive(:perform_search).with('query', '1').and_return([])
      end

      # Test case description
      it 'renders search template' do
        get :search, params: { page: '1', query: 'query' }
        expect(response).to render_template('prompts/index')
      end
    end

    # Context when an error occurs
    context 'when an error occurs' do
      # Before each test case in this context
      before do
        allow(prompt_service).to receive(:perform_search).and_raise(PromptServiceException)
      end

      # Test case description
      it 'renders search template with flash alert' do
        get :search, params: { page: '1', query: 'query' }
        expect(flash[:alert]).to be_present
        expect(response).to render_template('index')
      end
    end
  end
end