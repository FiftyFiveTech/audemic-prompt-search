class ApplicationController < ActionController::Base
  add_flash_types :info, :error, :warning

  def not_found_routes
    render file: Rails.public_path.join('routes_mismatched.html'), status: :not_found, layout: false
  end
end