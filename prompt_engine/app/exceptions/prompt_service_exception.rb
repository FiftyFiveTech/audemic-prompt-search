class PromptServiceException < StandardError
  def initialize(message = 'Internal error occurred', error=nil)
    message = "#{message}: #{error&.message}"
    super(message)
  end
end
