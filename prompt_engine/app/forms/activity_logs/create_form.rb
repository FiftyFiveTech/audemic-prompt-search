require_relative '../../repositories/activity_log_repository' 

module ActivityLogs
  # Form object for creating activity logs
  class CreateForm
    include ActiveModel::Model

    def initialize(search_keyword, repository = ActivityLogRepository.new)
      @repository = repository
      @search_keyword = search_keyword
    end

    # Accessor for the search keyword attribute
    attr_accessor :search_keyword, :repository

    # Validation for the presence of search keyword
    validates :search_keyword, presence: true

    # Method to persist the activity log
    def persist
      return false unless valid?

      # Create a new activity log record with the provided search keyword
      repository.LogActivity(search_keyword)
    end
  end
end
