require 'utils/retryable'
class PromptRepository
  include PromptHelper
  include Utils::Retryable 

  def initialize
    @client = ElasticsearchClient.client
  end

  def list
    begin
      parse_data(Prompt.search(query: { match_all: {} }, size: $DEFAULT_DATA_SIZE).response)
    rescue StandardError => error
      raise error
    end
  end

  def find(search_query)
    begin
      parse_data(Prompt.search(query: { match: {data: search_query}}, size: $DEFAULT_DATA_SIZE).response)
    rescue StandardError => error
      raise error
    end
  end

  def delete(index_name)
    begin
      with_retry do
        if @client.indices.exists(index: 'prompts')
          Prompt.__elasticsearch__.delete_index!
        end
      end
    rescue StandardError => error
      raise error                
    end
  end

  def save(bulk_request_body)
    begin
      with_retry do
        # Send bulk request to ElasticSearch
        response = @client.bulk(body: bulk_request_body)
        handle_response(response)
      end
    rescue StandardError => error
      raise error
    end
  end

  private

  def handle_response(response)
    # Check if response has errors
    if response['errors'] == true
      Rails.logger.error("Bulk indexing errors: #{response['errors']}.")  
    end
  end
end
