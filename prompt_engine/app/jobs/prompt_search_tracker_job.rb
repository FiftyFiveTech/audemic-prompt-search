class PromptSearchTrackerJob < ApplicationJob
  queue_as :default

  def perform(search_query)
    begin
      form = ActivityLogs::CreateForm.new(search_query)
      form.persist
    rescue StandardError => error
      raise "Error creating activity log: #{error}"
    end
  end
end
