# Project Name
Audemic Prompt Engine

**Important Notes:**
- Our project is hosted on AWS instead of Heroku due to cost considerations. Consolidating three services within a single Docker container helps minimize expenses, as deploying each service separately can be costly.
- Currently, we fetch only 10,000 records in multiple batches. If required, you can change the configuration through env variables.

### Description

The Audemic Prompt Engine makes it easy to find the perfect image prompts for your creative projects. Here's how it works:

### Database Seeding
We take a collection of image prompts and carefully add them to our database. This lays the foundation for efficient searching.
### Elasticsearch Indexing
We use Elasticsearch, a powerful search engine, to index the prompts.  Indexing means organizing the information for super-fast searches.
### Search Controller
We've built a specialized search system that communicates with Elasticsearch. This allows us to type in your desired image and get relevant results quickly.

## Features

### Prompt Indexing and Seeding
Easily import and organize large amounts of image prompts.

### Elasticsearch Integration
Leverages Elasticsearch's speed and flexibility for accurate search results.

### Search Interface
A simple web page with a search bar lets us find prompts effortlessly.

Search Tracker
Monitors user searches to help us understand how the engine is being used and how we might improve it.

## How to Use (Online)

1. Visit the Audemic Prompt Engine website: [audemic-prompt-app.fiftyfivetech.io]
2. In the search bar, type a description of the image we'd like to generate. For example: "a comic potrait of a female necromamcer"
3. The engine will quickly display image prompts that match your search.

## Local Setup (For Developers)

### Prerequisites

* Ruby (version 3.2.3)
* Rails (version 7.1.3.2)
* Elasticsearch (version 7.2)
* Docker

### Steps

1. **Clone the Repository:** Get the project code using Git (instructions on how to do this would be included in the actual README).
2. **Environment Variables:** Create a `.env` file to securely store project settings (see the template below for guidance).
3. **Dependencies:** Install required software libraries with `bundle install`.
4. **Database Setup:** 
    * `rails db:create`
    * `rails db:migrate`
    * `rails db:seed` (this imports our test set of image prompts)
    * `rails rails spec`
5. **Start Servers:**
     * `service postgresql start`
     * `systemctl start elasticsearch.service`
6. **Run Application:**  `rails server`
7. **Access Locally:** Open http://localhost:3000 in your browser. 

 ### .env File Template
```
AUDEMIC_PROMPT_ENGINE_DATABASE_PASSWORD=audemic123
SECRET_KEY_BASE=89a3e9dc9925a7e37e09b96aa44f0869
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_HOST=localhost
POSTGRES_DB=postgres
ELASTICSEARCH_URL=http://localhost:9200
ELASTIC_SEARCH_PASSWORD=1PxMVQIqGI7k7+CHaJNC
DEFAULT_DATA_SIZE=10000
PAGINATION_SIZE=10

ELASTIC_USERNAME=elastic
ELASTIC_PASSWORD=1PxMVQIqGI7k7+CHaJNC
xpack.security.enabled=true
xpack.security.transport.ssl.enabled=false
discovery.type=single-node
ES_JAVA_OPTS=-Xms512m -Xmx512m
 
SERVER_HOST_NAME=audemic-prompt-app.fiftyfivetech.io 
```

### Running with Docker

For a simplified setup, use Docker to run the project on your system:

Make sure you have Docker installed.
Build and run the containers:

- docker build -t ror:v1 .
- docker run -it --rm --name ror -p 3000:3000 --env-file .env ror:v1

**Note:** Set SERVER_HOST_NAME=0.0.0.0 in env file in case you are running docker container

### How to Use

1. Access the search page for prompt- https://audemic-prompt-app.fiftyfivetech.io
1. Search data like:
    - "a portrait"
    - "an intricate painting of divali celebrations"
    - "fantasy painting with a wizard"
    - "a very beautiful anime cute girl"